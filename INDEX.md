# Bin2Asm

simple program to convert a binary file to assembly language data


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## BIN2ASM.LSM

<table>
<tr><td>title</td><td>Bin2Asm</td></tr>
<tr><td>version</td><td>2021-09-20</td></tr>
<tr><td>entered&nbsp;date</td><td>2021-09-20</td></tr>
<tr><td>description</td><td>simple program to convert a binary file to assembly language data</td></tr>
<tr><td>keywords</td><td>dos devel</td></tr>
<tr><td>author</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Jerome Shidel &lt;jerome _AT_ shidel.net&gt;</td></tr>
<tr><td>alternate&nbsp;site</td><td>https://gitlab.com/DOSx86/bin2asm</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[BSD 3-Clause License](LICENSE)</td></tr>
</table>
